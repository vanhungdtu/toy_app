class User < ApplicationRecord
  # before_save là một phương thức dạng callback
  # sẽ được gọi tại một thời điểm cụ thể nào đó trong vòng đời sống của một đối tượng Active Record
  # Trong trường hợp này, thời điểm cụ thể chính là trước khi đối tượng được lưu
  # self ở đây được hiểu là đối tượng User
  # before_save { self.email = self.email.downcase }
  before_save { self.email.downcase! }
  # Tên không được bỏ trống, dài tối đa 50 ký tự
  validates :name, presence: true, length: { maximum: 50}
  # Biểu thức hợp lệ dùng để kiểm tra e-mail
  VALID_EMAIL_REGEX = /\A[\w+\-.]+@[a-z\d\-.]+\.[a-z]+\z/i
  # E-mail không được bỏ trống, dài tối đa 255 ký tự, email phải là duy nhất không phân biệt hoa thường
  validates :email, presence: true, length: { maximum: 255 }, format: { with: VALID_EMAIL_REGEX },
                    uniqueness: { case_sensitive: false }
  # Phương thức này sẽ lưu một password đã được hash (đã được mã hóa) vào trong thuộc tính password_digest của một model nào đó
  # Tạo một cặp thuộc tính ảo password and password_confirmation (dùng cho việc tạo và xác thực mật khẩu ở trang đăng ký)
  # Đồng thời nó cũng cung cấp một phương thức được gọi là authenticate trả về user khi password truyền vào chính xác
  has_secure_password
  validates :password, presence: true, length: { minimum: 8 }
end
