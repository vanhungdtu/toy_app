class AddIndexToUsersEmail < ActiveRecord::Migration[5.1]
  def change
    # Phương thức add_index được dùng để đánh chỉ mục cho cột
    add_index :users, :email, unique: true
  end
end
